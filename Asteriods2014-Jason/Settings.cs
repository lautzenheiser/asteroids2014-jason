﻿namespace Asteriods2014_Jason
{
  public static class Settings
  {
    public static bool ShowBoundingRectangles { get { return false; } }
    public static bool ShowFrameRate { get { return true; } }
  }
}