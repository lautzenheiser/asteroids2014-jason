﻿using System;
using Microsoft.Xna.Framework;

namespace Asteriods2014_Jason
{
  public static class MathUtilities
  {
    public static Vector2 FromPolar(float angle, float magnitude)
    {
      return magnitude * new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
    } 
  }
}