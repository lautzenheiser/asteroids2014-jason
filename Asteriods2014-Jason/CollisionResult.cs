﻿using Microsoft.Xna.Framework;

namespace Asteriods2014_Jason
{
  public struct CollisionResult
  {
    public bool HasCollided { get; set; }
    public Vector2 CollisionPoint { get; set; }
  }
}