﻿#region Using Statements

using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

#endregion

namespace Asteriods2014_Jason
{
  /// <summary>
  /// This is the main type for your game
  /// </summary>
  public class GameCore : Game
  {
    public static GameCore Instance { get; private set; }
    public static Viewport Viewport { get { return Instance.GraphicsDevice.Viewport; } }
    public static Vector2 ScreenSize {get {return new Vector2(Viewport.Width, Viewport.Height);}}
    public static GameTime GameTime { get; private set; }
    public static ParticleManager<ParticleState> ParticleManager { get; private set; } 

    readonly GraphicsDeviceManager graphics;
    SpriteBatch spriteBatch;

    private FontRenderer fontRenderer;
    QuadTree quadTree;
    private bool gamePaused;
    private KeyboardState lastState;

    private TimeSpan pausePosition;

    public GameCore()
    {
      Instance = this;
      graphics = new GraphicsDeviceManager(this);
      Content.RootDirectory = "Content";

      graphics.PreferredBackBufferWidth = 1280;
      graphics.PreferredBackBufferHeight = 1024;

    }

    protected override void Initialize()
    {
      base.Initialize();

      ParticleManager = new ParticleManager<ParticleState>(1024*20, ParticleState.UpdateParticle);

      quadTree = new QuadTree(0, GraphicsDevice.Viewport.Bounds);

      EntityManager.Add(new Starfield());
      EntityManager.Add(Ship.Instance);

      var fontFilePath = Path.Combine(Content.RootDirectory, "font.fnt");

        using (var stream = TitleContainer.OpenStream(fontFilePath))
        {
            var fontFile = FontLoader.Load(stream);
            var fontTexture = Content.Load<Texture2D>("font_0");
            fontRenderer = new FontRenderer(fontFile, fontTexture);
            stream.Close();
        }


      var song = Song.FromUri("Music", new Uri("content/Music.mp3",UriKind.Relative));
//      var song = Content.Load<Song>("Music.xnb");  
      MediaPlayer.IsRepeating = true;
      MediaPlayer.Volume = .3f;
      MediaPlayer.Play(song);

    }

    protected override void LoadContent()
    {
      // Create a new SpriteBatch, which can be used to draw textures.
      spriteBatch = new SpriteBatch(GraphicsDevice);
      Art.Load(Content);
      SoundEffects.Load(Content);
      Font.Load(Content);

//      asteroids.StartLevel();

    }

    protected override void Update(GameTime gameTime)
    {
      GameTime = gameTime;

      if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
        Exit();

      KeyboardState keyboardState = Keyboard.GetState();

      if (keyboardState.IsKeyDown(Keys.P) && lastState.IsKeyUp(Keys.P))
      {
        gamePaused = !gamePaused;
        if (gamePaused)
        {
          pausePosition = MediaPlayer.PlayPosition;
          MediaPlayer.Stop();
        }
        else
        {
          MediaPlayer.Resume();
        }
      }

      lastState = keyboardState;


      if (!gamePaused)
      {
        EntityManager.Update();
        EnemySpawner.Update();
        ParticleManager.Update();
        PlayerStatus.Update();
      }

      base.Update(gameTime);
    }


    protected override void Draw(GameTime gameTime)
    {
      GraphicsDevice.Clear(Color.Black);

      spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive);
      ParticleManager.Draw(spriteBatch);
      spriteBatch.End();

      spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
      EntityManager.Draw(spriteBatch);

      spriteBatch.DrawString(Font.Arial20, string.Format("Lives: {0}", PlayerStatus.Lives), new Vector2(20, 10), Color.Aqua);
      spriteBatch.DrawString(Font.Arial20, string.Format("Score: {0}", PlayerStatus.Score), new Vector2(20, 30), Color.Aqua);

      if (gamePaused)
      {
        spriteBatch.DrawString(Font.Arial20, "Game Paused", new Vector2(Viewport.Width/2 - 65, 10), Color.Red);
      }
      

      spriteBatch.End();

      base.Draw(gameTime);
    }


  }
}
